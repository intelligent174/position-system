import requests
from xml.dom.minidom import parseString
from config.settings import (
    YANDEX_USER,
    YANDEX_USER_KEY,
)

from project.models import PositionRequestPydantic


class YandexPositionApi:
    def get_position(self, request: PositionRequestPydantic, page: int = 0, ) -> tuple:
        url = 'https://yandex.ru/search/xml?'

        params = {
            'user': YANDEX_USER,
            'key': YANDEX_USER_KEY,
            'query': request.query_keywords,
            'lr': request.region_id,
            'page': page,
            'groupby': 'attr=d.mode=deep.groups-on-page=100.docs-in-group=1',
        }

        response = requests.get(url, params=params).text
        dom_xml = parseString(response)
        error_tag = dom_xml.getElementsByTagName('error')[0] if dom_xml.getElementsByTagName('error') else None

        if error_tag is not None and error_tag.getAttribute("code") == '55':
            return self.get_position(request)

        url_tag_values = self.get_url_tag_values(dom_xml)

        return self.url_tag_parser(request.domain, url_tag_values)

    @staticmethod
    def get_url_tag_values(dom_xml) -> list:
        url_tags = dom_xml.getElementsByTagName('url')

        return [tag.childNodes[0].nodeValue for tag in url_tags]

    @staticmethod
    def url_tag_parser(site_domain: str, url_tag_values: list) -> tuple:
        position_url_site = tuple()

        for position, url_site in enumerate(url_tag_values, start=1):
            if site_domain in url_site.lower():
                position_url_site += position, url_site

                return position_url_site

        if not position_url_site:
            return 0, site_domain
