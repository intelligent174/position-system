import requests

from typing import List


class YandexLocationApi:
    def get_regions(self, city: str, country: str, max_results: int = 20) -> List[dict]:
        url = 'http://suggest-maps.yandex.ru/suggest-geo'
        params = {
            'search_type': 'tune',
            'v': '9',
            'results': max_results,
            'lang': 'ru',
            'part': f'{country} {city}'.strip()
        }
        response = requests.get(url, params=params).json()

        return [self._make_region(region) for region in response['results']]

    @classmethod
    def _make_region(cls, region: dict) -> dict:
        return {
            'title': region.get('title', ''),
            'subtitle': region.get('subtitle', ''),
            'region': region.get('geoid'),
        }
