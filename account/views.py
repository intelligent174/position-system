from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
)
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView

from account.forms import AccountRegistrationForm
from account.models import CustomUser


class RegisterAccountView(CreateView):
    model = CustomUser
    template_name = 'account/register_user.html'
    form_class = AccountRegistrationForm
    success_url = reverse_lazy('core:index')

    def form_valid(self, form):
        user = form.save(commit=False)
        user.save()
        # subject = f'{user.email} вы успешно зарегистрировались на сайте'
        # message = f'Здравствуйте, {user.email}!'
        # send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [user.email, ])
        return HttpResponseRedirect(self.success_url)


class AccountLoginView(LoginView):
    template_name = 'account/login.html'

    def get_success_url(self):
        return reverse_lazy('core:index')


class AccountLogoutView(LoginRequiredMixin, LogoutView):
    template_name = 'account/logout.html'


class AccountPasswordChangeView(SuccessMessageMixin, LoginRequiredMixin, PasswordChangeView):
    template_name = 'account/password_change.html'
    success_url = reverse_lazy('core:index')
    success_message = 'Пароль пользователя изменен'
