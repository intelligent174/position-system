from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from account.models import CustomUser


class AccountCreationForm(forms.ModelForm):
    """
    Формы для создания новых пользователей.
    """
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput)

    class Meta:
        model = CustomUser
        fields = ('email',)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class AccountEditForm(forms.ModelForm):
    """
    Форма для обновления пользователей.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser
        fields = ('email', 'password', 'date_joined', 'is_active',)

    def clean_password(self):
        return self.initial['password']


class AccountRegistrationForm(forms.ModelForm):
    """
    Форма для регистрации пользователей.
    """
    email = forms.EmailField(
        required=True,
        label='Адрес электронной почты'
    )
    password = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput
    )

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user

    class Meta:
        model = CustomUser
        fields = ('email', 'password')
