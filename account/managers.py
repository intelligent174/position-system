from django.contrib.auth.models import BaseUserManager


class CustomUserManager(BaseUserManager):
    """
    Пользовательский менеджер модели пользователя,
    где поле email является уникальным идентификатором.
    для аутентификации вместо поля username.
    """

    def create_user(self, email, password, **extra_fields):
        """
        Создает и сохраняет пользователя с указанным
         адресом электронной почты и паролем.
        """
        if not email:
            raise ValueError('У пользователей должен быть адрес электронной почты')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Создает и сохраняет суперпользователя с указанным адресом
         электронной почты и паролем.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError('У суперпользователя поле is_staff = True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('У суперпользователя поле is_superuser=True.')
        return self.create_user(email, password, **extra_fields)
