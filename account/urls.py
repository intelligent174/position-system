from django.urls import path

from account.views import (
    AccountLoginView,
    AccountLogoutView,
    RegisterAccountView,
)

app_name = 'account'
urlpatterns = [
    path('register/', RegisterAccountView.as_view(), name='register'),
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
]
