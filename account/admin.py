from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group

from account.models import CustomUser
from account.forms import AccountCreationForm, AccountEditForm


@admin.register(CustomUser)
class AccountAdmin(BaseUserAdmin):
    """
    Форма для добавления и изменения пользователей.
    """
    form = AccountEditForm
    add_form = AccountCreationForm

    list_display = ('id', 'email', 'date_joined',)
    list_filter = ('date_joined',)
    list_display_links = ('email',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Персональная информация', {'fields': ('date_joined',)}),
        ('Права пользователя', {'fields': ('is_active',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password',)}
         ),
    )
    list_per_page = 10
    show_full_result_count = False
    date_hierarchy = 'date_joined'
    search_fields = ('email',)
    ordering = ('-id',)
    filter_horizontal = ()


# Для чего нужно убирать эту модель ?
admin.site.unregister(Group)
