export function setParamsInUrl(urlObject) {

    if ('url_path' in urlObject) {
        let baseUrl = window.location.origin;
        let url = urlObject.url_path;
        delete urlObject.url_path;
        let requestURL = new URL(url, baseUrl);

        for (let key in urlObject) {
            requestURL.searchParams.set(key, urlObject[key]);
        }
        return requestURL
    }
}
