import {sendRequest} from "./send-request.js"
import {CreateObject} from './create-object-or-element-or-form.js'
import {setParamsInUrl} from "./set-params-in-url.js";
import {convertDataToObject, createTableWithData} from "./details-of-selected-project.js"


export function startCheckingSitePositions() {
    let url;

    if (this.classList.contains('js-get-positions')) {
        url = new CreateObject({
            url_path: this.getAttribute("data-url"),
            project_slug: this.getAttribute('data-project-slug'),
        });

        if (this.hasAttribute('data-running_check_status')) {
            url.running_check_status = this.getAttribute('data-running_check_status')
        }

        let projectDataFilteringForm = document.getElementById('projectDataFilteringForm');

        for (let radioButton of projectDataFilteringForm) {
            if (radioButton.checked) {
                url[radioButton.name] = radioButton.value;
            }
        }
    }
    let requestURL = setParamsInUrl(url);
    let requestOptions = new CreateObject({
        method: 'GET',
        url: requestURL,
    });

    sendRequest(requestOptions)
        .then(siteRatings => {
            if (siteRatings.status !== 200) {
                let keywordGroup = convertDataToObject(siteRatings);
                createTableWithData(keywordGroup)
            }
        })
        .catch(err => console.log(err))
}
