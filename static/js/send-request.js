const requestHeaders = new Headers();

export async function sendRequest(requestOptions) {
    try {
        let init = {};

        if (requestOptions.method === 'POST') {
            Object.entries(requestOptions.headers).forEach(([key, value]) => {
                requestHeaders.set(key, value);
            });

            init = {
                method: requestOptions.method,
                body: requestOptions.body,
                headers: requestHeaders,
            };
        }

        const request = new Request(requestOptions.url, init);

        return await fetch(request)
            .then(response => response.json());

    } catch (err) {
        console.log(err);
        return await Promise.reject(err)
    }
}