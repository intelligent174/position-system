import {getFormValues} from './get-form-values.js'
import {getCookie} from './get-cookie.js'
import {sendRequest} from './send-request.js'
import {startDeletingForSelectedItem} from './start-deleting-for-selected-item.js'
import {CreateObject, CreateForm, CreateElement} from './create-object-or-element-or-form.js'
import {setParamsInUrl} from "./set-params-in-url.js";

// При активации CSRF_USE_SESSIONS или CSRF_COOKIE_HTTPONLY csrftoken нужно
// получать другим способом см. документацию:
const csrftoken = getCookie('csrftoken');
let formForCreatingKeywords = document.getElementById('form_id_for_add_keywords');


export function createFormToAddGroupsOfKeywords(e) {
    let valueOfSelectedOption = this.options[this.selectedIndex].value;
    let blockForGeneratedForm = document.getElementById('id_create_keyword_group');
    let requestURL = blockForGeneratedForm.dataset['url'];
    let input = new CreateObject({
        element: 'input',
        id: 'idKeywordGroup',
        name: 'keywordGroupName',
        class: 'col-md-4 col-sm-12 form-control my-2',
        type: 'text',
        required: 'required',
    });
    let button = new CreateObject({
        element: 'button',
        id: 'idSubmitButton',
        class: 'col-4 btn btn-success align-self-center my-2',
        type: 'submit',
        text: 'Применить',
    });
    let label = new CreateObject({
        element: 'label',
        for: 'idKeywordGroup',
        text: 'Введите название группы ключевых слов:',
        class: 'col-form-label w-100 text-center',
    });
    let form = new CreateObject({
        element: 'form',
        method: 'POST',
        id: 'idFormCreatingGroupOfKeywords',
        class: 'd-flex flex-column form-control',
    });

    if (valueOfSelectedOption === "-1") {

        let formCreatingGroupsOfKeywords = new CreateForm(
            form,
            label,
            input,
            button,
        );
        blockForGeneratedForm.innerHTML = '';
        blockForGeneratedForm.appendChild(formCreatingGroupsOfKeywords);

        formCreatingGroupsOfKeywords.addEventListener('submit', (e) => {
            e.preventDefault();
            let formValues = getFormValues(formCreatingGroupsOfKeywords);
            const requestOptions = new CreateObject({
                method: 'POST',
                url: requestURL,
                headers: {
                    'X-CSRFToken': csrftoken,
                    'Content-type': 'application/json',
                },
                body: JSON.stringify(formValues),
            });

            sendRequest(requestOptions)
                .then(keywordGroups => {
                    let selectElement = document.getElementById('id_keyword_groups');
                    selectElement.innerHTML = '';

                    for (let keywordGroup of keywordGroups) {
                        let option = new Option(keywordGroup.name, keywordGroup.id);
                        selectElement.appendChild(option)
                    }
                    let option = new CreateObject({
                        element: 'option',
                        id: 'createGroup',
                        value: '-1',
                        text: 'Создать группу'
                    });
                    let defaultOption = new CreateObject({
                        element: 'option',
                        value: '',
                        selected: '',
                        text: '---------'
                    });
                    let optionElement = new CreateElement(option);
                    let defaultOptionElement = new CreateElement(defaultOption);
                    selectElement.insertAdjacentElement('afterbegin', defaultOptionElement);
                    selectElement.appendChild(optionElement);
                    getKeywordGroupsAndKeywords()
                })
                .catch(err => console.log(err));
            // Удаляем созданную форму после отправки данных на сервер.
            blockForGeneratedForm.removeChild(formCreatingGroupsOfKeywords)
        });
    } else {
        let formCreatingGroupsOfKeywords = document.getElementById('idFormCreatingGroupOfKeywords');
        // Удаляем форму для создания группы ключевых слов, в случае выбора в поле для
        // выбора групп ключевых слов конкретной группы, а не пункта "создать группу".
        if (formCreatingGroupsOfKeywords) {
            blockForGeneratedForm.removeChild(formCreatingGroupsOfKeywords)
        }
    }
}


if (formForCreatingKeywords) {
    formForCreatingKeywords.addEventListener('submit', (e) => {
        e.preventDefault();
        let formValues = getFormValues(formForCreatingKeywords);
        const requestURL = formForCreatingKeywords.getAttribute('action');
        const requestOptions = new CreateObject({
            method: 'POST',
            url: requestURL,
            headers: {
                'X-CSRFToken': csrftoken,
                'Content-type': 'application/json',
            },
            body: JSON.stringify(formValues),
        });

        if (formValues.keyword_groups !== '-1') {
            sendRequest(requestOptions)
                .then(projectKeywordGroups => {
                    createListWithElementsInside(projectKeywordGroups);
                    startDeletingForSelectedItem()
                })
                .catch(err => console.log(err));

            formForCreatingKeywords.reset();
        }
    });
}


function createListWithElementsInside(listOfKeywordGroups) {
    let keywordGroups = document.getElementById('keyword_groups_list_id');
    keywordGroups.innerHTML = '';

    listOfKeywordGroups.forEach(groups => {
        let li = new CreateObject({
            element: 'li',
            class: 'd-flex justify-content-between',
        });
        let divWithKeywordGroup = new CreateObject({
            element: 'div',
            text: `${groups.name} :`,
            class: 'my-1 p-1',
        });
        let divWithButton = new CreateObject({
            element: 'div',
        });
        let anchor = new CreateObject({
            element: 'a',
            type: 'button',
            class: 'btn btn-outline-danger btn-sm js-remove-keyword-group',
            text: 'Удалить',
            ['data-keyword-group-id']: groups.id,
        });
        let liElement = new CreateElement(li);
        let divWithKeywordGroupElement = new CreateElement(divWithKeywordGroup);
        let divWithButtonElement = new CreateElement(divWithButton);
        let anchorElement = new CreateElement(anchor);
        divWithButtonElement.appendChild(anchorElement);
        liElement.appendChild(divWithKeywordGroupElement);
        liElement.appendChild(divWithButtonElement);
        keywordGroups.appendChild(liElement);

        if (groups.keywords) {
            let ul = new CreateObject({
                element: 'ul',
            });
            let ulElement = new CreateElement(ul);
            liElement.appendChild(ulElement);

            for (let keyword of groups.keywords) {
                let li = new CreateObject({
                    element: 'li',
                    class: 'd-flex justify-content-between my-2',
                });
                let divWithKeyword = new CreateObject({
                    element: 'div',
                    text: keyword.name,
                    class: 'my-1 p-1',
                });
                let divWithButton = new CreateObject({
                    element: 'div',
                });
                let anchor = new CreateObject({
                    element: 'a',
                    type: 'button',
                    class: 'btn btn-outline-danger btn-sm js-remove-keyword',
                    text: 'Удалить',
                    ['data-keyword-id']: `${keyword.id}`,
                });
                let liElement = new CreateElement(li);
                let divElement = new CreateElement(divWithKeyword);
                let divWithButtonElement = new CreateElement(divWithButton);
                let anchorElement = new CreateElement(anchor);
                divWithButtonElement.appendChild(anchorElement);
                liElement.appendChild(divElement);
                liElement.appendChild(divWithButtonElement);
                ulElement.appendChild(liElement);
                keywordGroups.appendChild(ulElement)
            }
        }
    })
}


function getKeywordGroupsAndKeywords() {
    let keywordGroupsWithKeywords = document.getElementById('listOfKeywordGroupsWithKeywords');
    let slug = keywordGroupsWithKeywords.getAttribute('data-project-slug');
    let urlData = keywordGroupsWithKeywords.getAttribute('data-url');
    let urlObject = new CreateObject({
        url_path: urlData,
        slug: slug,
    });
    let requestURL = setParamsInUrl(urlObject);
    let requestOptions = new CreateObject({
        method: 'GET',
        url: requestURL
    });

    sendRequest(requestOptions)
        .then(projectKeywordGroups => {
            createListWithElementsInside(projectKeywordGroups);
            startDeletingForSelectedItem();
        })
        .catch(err => console.log(err))
}


export function removeKeywordOrGroupOfKeywords() {
    let keywordGroups = document.getElementById('keyword_groups_list_id');
    let requestURL = keywordGroups.getAttribute('data-url');
    // Уточнить по такому подходу let objectToDelete??
    let objectToDelete;
    if (this.classList.contains('js-remove-keyword')) {
        let keywordId = this.getAttribute('data-keyword-id');
        objectToDelete = new CreateObject({
            keyword_id: keywordId,
        });
    } else if (this.classList.contains('js-remove-keyword-group')) {
        let keywordGroupId = this.getAttribute('data-keyword-group-id');
        objectToDelete = new CreateObject({
            keyword_group_id: keywordGroupId,
        });
    }

    let requestOptions = new CreateObject({
        method: 'POST',
        url: requestURL,
        headers: {
            'X-CSRFToken': csrftoken,
            'Content-type': 'application/json',
        },
        body: JSON.stringify(objectToDelete)
    });

    sendRequest(requestOptions)
        .then(response => {
            // Уточнить нужно ли здесь делать эту проверку ??
            if (response.status === 200) {
                getKeywordGroupsAndKeywords()
            }
        })
        .catch(err => console.log(err))
}
