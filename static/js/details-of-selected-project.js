import {CreateElement, CreateObject} from "./create-object-or-element-or-form.js"
import {sendRequest} from "./send-request.js"
import {setParamsInUrl} from "./set-params-in-url.js"


export function filterProjectData(event) {
    let urlPath = this.getAttribute('action');
    let projectSlug = this.getAttribute('data-project-slug');
    let url = new CreateObject({
        url_path: urlPath,
        project_slug: projectSlug
    });

    for (let radioButton of this) {

        if (radioButton.checked) {
            url[radioButton.name] = radioButton.value;
        }
    }

    let requestURL = setParamsInUrl(url);
    let requestOptions = new CreateObject({
        method: 'GET',
        url: requestURL,
    });

    sendRequest(requestOptions)
        .then(siteRatings => {
                if (siteRatings.length !== 0) {
                    let keywordGroup = convertDataToObject(siteRatings);
                    let selectedRadioButton = event.target;

                    createSearchSystemKeywordGroups(keywordGroup, selectedRadioButton);
                    createTableWithData(keywordGroup)
                }
            }
        )
        .catch(err => console.log(err))
}


export function convertDataToObject(siteRatings) {
    let keywordGroup = new CreateObject({
        keywords: [],
    });

    for (let siteRating of siteRatings) {
        if (!keywordGroup.hasOwnProperty('keyword_group')) {
            keywordGroup.keyword_group = siteRating.keyword_group;
            keywordGroup.search_system = siteRating.search_system;
            keywordGroup.keyword_groups = siteRating.keyword_groups;

        }
        let foundKeywordRatings = keywordGroup.keywords.find(keywordRating => {
            return keywordRating.keyword === siteRating.keywords
        });
        let rating = new CreateObject({
            position: siteRating.position,
            date: siteRating.date_of_check,
            position_difference: siteRating.position_difference,
        });

        if (!foundKeywordRatings) {
            let keywordRatings = new CreateObject({
                keyword: siteRating.keywords,
                url_site: siteRating.url_site,
                ratings: [],
            });
            keywordRatings.ratings.push(rating);
            keywordGroup.keywords.push(keywordRatings);
        } else {
            foundKeywordRatings.ratings.push(rating);
        }
    }
    return keywordGroup
}


function createSearchSystemKeywordGroups(keywordGroup, selectedRadioButton) {
    let setOfKeywordGroupFields = document.getElementById('search_system_keyword_groups');

    if (selectedRadioButton.checked && selectedRadioButton.name === 'search_system') {
        setOfKeywordGroupFields.innerText = '';
        let legendObject = new CreateObject({
            element: 'legend',
            text: 'Группы:',
            class: 'col-form-label col-1'
        });
        let div = new CreateObject({
            element: 'div',
            class: 'col-sm-8',
        });
        let elementLegend = new CreateElement(legendObject);
        let divElement = new CreateElement(div);
        let searchSystemKeywordGroups = Object.entries(keywordGroup.keyword_groups);
        setOfKeywordGroupFields.appendChild(elementLegend);

        for (let [index, group] of searchSystemKeywordGroups) {
            let labelObject = new CreateObject({
                element: 'label',
                for: `keyword_group_id_${group.id}`,
                text: group.name,
                class: 'col-form-label',
            });
            let inputObject = new CreateObject({
                element: 'input',
                type: 'radio',
                name: 'keyword_group',
                value: group.id,
                id: `keyword_group_id_${group.id}`,
                class: 'form-check-input',
            });
            let divForRadioButton = new CreateObject({
                element: 'div',
                class: 'form-check form-check-inline',
            });
            if (index === '0') {
                inputObject.checked = 'checked';
            }
            let elementInput = new CreateElement(inputObject);
            let elementLabel = new CreateElement(labelObject);
            let divForRadioButtonElement = new CreateElement(divForRadioButton);
            divForRadioButtonElement.appendChild(elementLabel);
            divForRadioButtonElement.appendChild(elementInput);
            divElement.appendChild(divForRadioButtonElement);
            setOfKeywordGroupFields.appendChild(divElement);
        }
    }
}


export function createTableWithData(keywordGroup) {

    let siteRatingsTable = document.getElementById('siteRatingsTableId');
    siteRatingsTable.innerText = '';

    let table = new CreateObject({
        element: 'table',
        class: 'table table-hover table-borderless table-sm',
    });
    let thead = new CreateObject({
        element: 'thead',
    });
    let tbody = new CreateObject({
        element: 'tbody',
    });
    let trHeader = new CreateObject({
        element: 'tr',
    });
    let tdWebPage = new CreateObject({
        element: 'td',
        text: 'Страница',
        style: 'background-color: #E9F2FD'
    });
    let tdKeyword = new CreateObject({
        element: 'td',
        text: 'Ключевая фраза',
        style: 'background-color: #E9F2FD'
    });

    let tdWebPageElement = new CreateElement(tdWebPage);
    let tdKeywordElement = new CreateElement(tdKeyword);
    let tableElement = new CreateElement(table);
    let theadElement = new CreateElement(thead);
    let trHeaderElement = new CreateElement(trHeader);
    let tbodyElement = new CreateElement(tbody);
    trHeaderElement.appendChild(tdWebPageElement);
    trHeaderElement.appendChild(tdKeywordElement);
    theadElement.appendChild(trHeaderElement);
    tableElement.appendChild(theadElement);
    siteRatingsTable.appendChild(tableElement);

    for (let keyword of keywordGroup.keywords) {
        let tdUrl = new CreateObject({
            element: 'td',
            text: `${keyword.url_site}`,
            class: 'text-nowrap',
        });
        let tdKeyword = new CreateObject({
            element: 'td',
            text: `${keyword.keyword}`,
            class: 'text-nowrap',
        });
        let trBody = new CreateObject({
            element: 'tr',
        });
        let tdUrlElement = new CreateElement(tdUrl);
        let tdKeywordElement = new CreateElement(tdKeyword);
        let trBodyElement = new CreateElement(trBody);
        trBodyElement.appendChild(tdUrlElement);
        trBodyElement.appendChild(tdKeywordElement);

        for (let rating of keyword.ratings) {
            let divPosition = new CreateObject({
                element: 'div',
                text: `${rating.position}`,
                class: 'ps-2 pe-1',
            });
            let tdBody = new CreateObject({
                element: 'td',
                class: 'px-0'
            });
            let divBody = '';
            let divPositionDifference;

            if (rating.position === 0) {
                divBody = new CreateObject({
                    element: 'div',
                    class: 'd-flex color-pos-0',
                })
            }

            if (0 < rating.position && rating.position <= 5) {
                divBody = new CreateObject({
                    element: 'div',
                    class: 'd-flex color-pos-1-5',
                })
            }

            if (6 <= rating.position && rating.position <= 12) {
                divBody = new CreateObject({
                    element: 'div',
                    class: 'd-flex color-pos-6-12',
                })
            }

            if (13 <= rating.position && rating.position <= 19) {
                divBody = new CreateObject({
                    element: 'div',
                    class: 'd-flex color-pos-13-19',
                })
            }

            if (20 <= rating.position && rating.position <= 29) {
                divBody = new CreateObject({
                    element: 'div',
                    class: 'd-flex color-pos-20-29',
                })
            }

            if (30 <= rating.position && rating.position <= 39) {
                divBody = new CreateObject({
                    element: 'div',
                    class: 'd-flex color-pos-30-39',
                })
            }

            if (40 <= rating.position && rating.position <= 49) {
                divBody = new CreateObject({
                    element: 'div',
                    class: 'd-flex color-pos-40-49',
                })
            }

            if (50 <= rating.position && rating.position <= 100) {
                divBody = new CreateObject({
                    element: 'div',
                    class: 'd-flex color-pos-50-100',
                })
            }

            if (rating.position_difference > 0) {
                divPositionDifference = new CreateObject({
                    element: 'div',
                    style: 'font-size:65%',
                    text: `+${rating.position_difference}`
                })
            }

            if (rating.position_difference === 0 || !rating.position_difference) {
                divPositionDifference = new CreateObject({
                    element: 'div',
                    style: 'font-size:65%',
                    text: '-'
                })
            }

            if (rating.position_difference < 0) {
                divPositionDifference = new CreateObject({
                    element: 'div',
                    style: 'font-size:65%',
                    text: `${rating.position_difference}`
                })
            }

            let divPositionElement = new CreateElement(divPosition);
            let tdBodyElement = new CreateElement(tdBody);
            let divBodyElement = new CreateElement(divBody);
            let divPositionDifferenceElement = new CreateElement(divPositionDifference);
            divBodyElement.appendChild(divPositionElement);
            divBodyElement.appendChild(divPositionDifferenceElement);
            tdBodyElement.appendChild(divBodyElement);
            let childNodes = Array.from(trHeaderElement.childNodes);
            let foundChildNode = childNodes.find(childNode => {
                return childNode.innerText === rating.date
            });

            if (!foundChildNode) {
                let tdDate = new CreateObject({
                    element: 'td',
                    class: 'px-0',
                    style: 'background-color: #E9F2FD'
                });
                let divDate = new CreateObject({
                    element: 'div',
                    text: `${rating.date}`,
                    class: 'px-2',
                });
                let tdDateElement = new CreateElement(tdDate);
                let divDateElement = new CreateElement(divDate);
                tdDateElement.appendChild(divDateElement);
                trHeaderElement.appendChild(tdDateElement);
            }
            trBodyElement.appendChild(tdBodyElement)
        }

        tbodyElement.appendChild(trBodyElement);
        tableElement.appendChild(tbodyElement);
        siteRatingsTable.appendChild(tableElement);

    }
}
