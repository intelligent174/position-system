import {getCookie} from './get-cookie.js';
import {getFormValues} from "./get-form-values.js";
import {sendRequest} from "./send-request.js";
import {setParamsInUrl} from "./set-params-in-url.js";
import {CreateElement, CreateObject} from "./create-object-or-element-or-form.js";
import {startDeletingForSelectedItem} from "./start-deleting-for-selected-item.js";


let csrftoken = getCookie('csrftoken');
let $cityInputField = $('#id_select_city');
let countryInputField = document.getElementById('id_country');
let searchSystemsContainer = document.getElementById('search_systems_list_id');


$cityInputField.autocomplete({
    appendTo: $('.search-results'),
    minLength: 2,
    delay: 500,
    focus: getNameOfRegion,
    select: getSelectedRegion,

    source: (request, response) => {
        let urlObject = new CreateObject({
            url_path: $cityInputField.data('url'),
            city: $cityInputField.val(),
            country: countryInputField.value,

        });
        let requestURL = setParamsInUrl(urlObject);
        let requestOptions = new CreateObject({
            method: 'GET',
            url: requestURL
        });

        sendRequest(requestOptions)
            .then(listRegions => {

                if ('error' in listRegions) {
                    let results = new CreateObject({
                        'value': `${listRegions.error}`

                    });
                    response(results);

                } else {
                    let results = $.map(listRegions, (region) => {
                        return new CreateObject({
                            'label': `${region.title} ${region.subtitle}`,
                            'value': region.region,
                        });
                    });
                    response(results);
                }
            })
            .catch(err => console.log(err));
    },


}).focus(function () {
    $(this).autocomplete('search', $(this).val())
});


function getNameOfRegion(e, ui) {
    e.preventDefault();
    let completeRegionData = ui.item.label;
    $cityInputField.val(completeRegionData)
}


function getSelectedRegion(e, ui) {
    e.preventDefault();
    let completeRegionData = ui.item.label;
    let cityId = ui.item.value;
    $('[name=city_id]').val(cityId);
    $cityInputField.val(completeRegionData);
}


$('#id_form_search_system').on('submit', (e) => {
    e.preventDefault();
    let formAddSearchSystem = document.getElementById('id_form_search_system');
    let formValues = getFormValues(formAddSearchSystem);
    let requestURL = formAddSearchSystem.getAttribute('action');
    let requestOptions = new CreateObject({
        method: 'POST',
        url: requestURL,
        headers: {
            'X-CSRFToken': csrftoken,
            'Content-type': 'application/json'
        },
        body: JSON.stringify(formValues),
    });

    sendRequest(requestOptions)
        .then(searchSystems => {
            createListOfSearchSystems(searchSystems);
            startDeletingForSelectedItem()
        })
        .catch(err => console.log(err));
    // очищаем форму добавления поисковой системы после ее отправки.
    formAddSearchSystem.reset();
});


function createListOfSearchSystems(searchSystems) {
    searchSystemsContainer.innerHTML = '';
    let ol = new CreateObject({
        element: 'ol'
    });
    let olElement = new CreateElement(ol);
    searchSystems.forEach(searchSystem => {
        let li = new CreateObject({
            element: 'li',
            class: 'd-flex justify-content-between my-2',
        });
        let div = new CreateObject({
            element: 'div',
            text: `${searchSystem.name}, ${searchSystem.city},`,
            class: 'my-1 p-1',
        });
        let divWithButton = new CreateObject({
            element: 'div',
        });
        let anchor = new CreateObject({
            element: 'a',
            class: 'btn btn-outline-danger btn-sm js-remove-search-system',
            type: 'button',
            ['data-search-system-id']: searchSystem.id,
            text: 'Удалить',
        });
        let liElement = new CreateElement(li);
        let divElement = new CreateElement(div);
        let divWithButtonElement = new CreateElement(divWithButton);
        let anchorElement = new CreateElement(anchor);
        divWithButtonElement.appendChild(anchorElement);
        liElement.appendChild(divElement);
        liElement.appendChild(divWithButtonElement);
        olElement.appendChild(liElement);
        searchSystemsContainer.appendChild(olElement)
    });
}

export function removeSearchSystem() {
    let requestURL = searchSystemsContainer.getAttribute('data-object-delete-url');
    let searchSystemId = this.getAttribute('data-search-system-id');
    let objectToDelete = new CreateObject({
        search_system_id: searchSystemId,
    });
    let requestOptions = new CreateObject({
        method: 'POST',
        url: requestURL,
        headers: {
            'X-CSRFToken': csrftoken,
            'Content-type': 'application/json',
        },
        body: JSON.stringify(objectToDelete)
    });

    sendRequest(requestOptions)
        .then(response => {
            if (response.status === 200) {
                let divSearchSystems = document.getElementById('search_systems');
                let slug = divSearchSystems.getAttribute('data-project-slug');
                let urlData = searchSystemsContainer.getAttribute('data-url');
                let urlObject = new CreateObject({
                    url_path: urlData,
                    slug: slug,
                });
                let requestURL = setParamsInUrl(urlObject);
                let requestOptions = new CreateObject({
                    method: 'GET',
                    url: requestURL
                });

                sendRequest(requestOptions)
                    .then(searchSystems => {
                        createListOfSearchSystems(searchSystems);
                        startDeletingForSelectedItem()
                    })
                    .catch(err => console.log(err))
            }
        })
        .catch(err => console.log(err))
}