import {CreateElement, CreateObject} from "./create-object-or-element-or-form.js";
import {sendRequest} from "./send-request.js";
import {setParamsInUrl} from "./set-params-in-url.js";


let inputFieldForSearchingProjects = document.getElementById('searching_projects');
let sidebar = document.getElementById('sidebar-id');
let sidebarForm = document.getElementById('sidebar_form_id');

if (inputFieldForSearchingProjects) {
    inputFieldForSearchingProjects.addEventListener('input', searchForUserProjects);
}


function searchForUserProjects() {

    let urlObject = new CreateObject({
        url_path: sidebarForm.getAttribute('action'),
        project_name: inputFieldForSearchingProjects.value
    });
    let requestURL = setParamsInUrl(urlObject);
    let requestOptions = new CreateObject({
        method: 'GET',
        url: requestURL
    });

    sendRequest(requestOptions)
        .then(userProjects => addProjectsToSidebar(userProjects))
        .catch(err => console.log(err));
}


function addProjectsToSidebar(userProjects) {

    sidebar.innerHTML = '';

    for (let [project_name, project_url] of Object.entries(userProjects)) {
        let liObject = new CreateObject({
            element: 'li',
            class: 'mt-1 p-1',
            style: 'list-style-type: none;'
        });

        let liElement = new CreateElement(liObject);

        let anchorObject = new CreateObject({
            element: 'a',
            href: project_url,
            text: project_name,
            style: 'color: #EEEEEE; text-decoration:none;',
        });

        let anchorElement = new CreateElement(anchorObject);

        liElement.appendChild(anchorElement);
        sidebar.appendChild(liElement)
    }
}

