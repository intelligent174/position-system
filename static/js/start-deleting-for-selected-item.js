import {removeKeywordOrGroupOfKeywords} from "./add_keywords_and_keyword_groups.js";
import {removeSearchSystem} from "./add_search_system.js";

let deleteKeywordsButtons = document.getElementsByClassName('js-remove-keyword-group');
let buttonsForDeletingKeywordGroups = document.getElementsByClassName('js-remove-keyword');
let searchSystemsDeleteButtons = document.getElementsByClassName('js-remove-search-system');


export function startDeletingForSelectedItem() {
    for (let button of deleteKeywordsButtons) {
        button.addEventListener('click', removeKeywordOrGroupOfKeywords)
    }

    for (let button of buttonsForDeletingKeywordGroups) {
        button.addEventListener('click', removeKeywordOrGroupOfKeywords)
    }

    for (let button of searchSystemsDeleteButtons) {
        button.addEventListener('click', removeSearchSystem)
    }
}