import './add_search_system.js'
import './search_project_in_user_projects.js'
import {createFormToAddGroupsOfKeywords} from './add_keywords_and_keyword_groups.js'
import {filterProjectData} from "./details-of-selected-project.js"
import {startCheckingSitePositions} from "./start-checking-site-positions.js"
import {startDeletingForSelectedItem} from "./start-deleting-for-selected-item.js";

document.addEventListener('DOMContentLoaded', () => {
    let buttonForGettingPositions = document.getElementsByClassName('js-get-positions');
    let projectDataFilteringForm = document.getElementById('projectDataFilteringForm');
    let selectedKeywordGroup = document.getElementById('id_keyword_groups');

    if (selectedKeywordGroup !== null) {
        selectedKeywordGroup.addEventListener('click', createFormToAddGroupsOfKeywords);
    }

    for (let button of buttonForGettingPositions) {
        button.addEventListener('click', startCheckingSitePositions)
    }

    if (projectDataFilteringForm !== null) {
        projectDataFilteringForm.addEventListener('input', filterProjectData);
    }

    startDeletingForSelectedItem();

});




