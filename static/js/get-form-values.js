export function getFormValues(form) {
    let formData = new FormData(form);
    let formValue = {};

    formData.forEach((value, key) => {
        formValue[key] = value
    });
    return formValue
}