import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy
from django.views.generic import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
)
from pydantic import ValidationError

from project.forms import (
    KeywordsAddingForm,
    ProjectAddingForm,
    ProjectEditingForm,
    SearchSystemsAddingForm,
)

from project.models import (
    Keywords,
    KeywordGroups,
    Project,
    SearchEnginePydantic,
    SearchSystem,
)

from project.repositories import (
    KeywordGroupsRepository,
    SiteRankingsRepository,
)

from project.services import (
    get_start_and_end_date,
    SearchSystemService,
    SiteRankingService,
    YandexLocationService,
    YandexLocationServiceError,
)


def add_keyword_group(request, slug: str):
    if request.method == 'POST':
        form_data = json.loads(request.body)
        keyword_group_name = form_data['keywordGroupName'].strip()
        current_project = Project.objects.prefetch_related('keywordgroups_set').get(slug=slug)
        keyword_groups = current_project.keywordgroups_set.all()
        converted_as_json = [keyword_group.as_json('id', 'name') for keyword_group in keyword_groups]
        keyword_group_as_json = KeywordGroups.objects.create(name=keyword_group_name,
                                                             project_id=current_project.id)
        converted_as_json.append(keyword_group_as_json.as_json('id', 'name'))

        return JsonResponse(converted_as_json, safe=False)


def get_filtered_project_data(request):
    check_period = request.GET.get('check_period')
    keyword_group_id = request.GET.get('keyword_group')
    search_system_id = request.GET.get('search_system')
    current_project_slug = request.GET.get('project_slug')

    if all([check_period, keyword_group_id, search_system_id, current_project_slug]):
        fields = ('position', 'keywords', 'url_site', 'date_of_check', 'search_system',
                  'keyword_group', 'keyword_groups', 'position_difference')
        period_dates = get_start_and_end_date(check_period)
        rankings_repository = SiteRankingsRepository()
        converted_site_ratings = rankings_repository.get_site_ratings(period_dates=period_dates,
                                                                      keyword_group_id=keyword_group_id,
                                                                      search_system_id=search_system_id,
                                                                      project_slug=current_project_slug,
                                                                      fields=fields)

        return JsonResponse(converted_site_ratings, safe=False)

    return JsonResponse({})


def get_keywords_and_keyword_groups_of_project(request):
    slug = request.GET.get('slug')
    if slug:
        keyword_groups = KeywordGroups.objects.prefetch_related('keywords_set').filter(project__slug=slug)
        converted_keyword_groups = [keyword_group.as_json('id', 'name', 'keywords') for keyword_group in
                                    keyword_groups]
        return JsonResponse(converted_keyword_groups, safe=False)
    return JsonResponse({})


def get_project_search_engines(request):
    slug = request.GET.get('slug')
    if slug:
        search_systems = SearchSystem.objects.filter(project__slug=slug)
        converted_search_systems = [search_system.as_json('id', 'name', 'city') for search_system in search_systems]
        return JsonResponse(converted_search_systems, safe=False)
    return JsonResponse({})


def get_regions(request):
    city = request.GET.get('city')
    country = request.GET.get('country', '')
    yandex_location_service = YandexLocationService()
    try:
        regions = yandex_location_service.get_regions(city, country)
        # Когда Яндеск ничего не возвращает выкидываем исключение. Уточнить ??
        if not regions:
            raise YandexLocationServiceError
        return JsonResponse(regions, safe=False)
    except YandexLocationServiceError:
        error_message = {
            'error': 'Проверьте корректность названия страны и региона.'
        }
        return JsonResponse(error_message, safe=False)


def remove_keyword_or_group_of_keywords(request):
    object_data_to_delete = json.loads(request.body)
    keyword_group_id = object_data_to_delete.get('keyword_group_id')
    keyword_id = object_data_to_delete.get('keyword_id')

    if keyword_id:
        keyword = Keywords.objects.get(id=keyword_id)
        keyword.delete()
        return JsonResponse({'status': 200})

    elif keyword_group_id:
        keyword_group = KeywordGroups.objects.get(id=keyword_group_id)
        keyword_group.delete()
        return JsonResponse({'status': 200})

    return JsonResponse({})


def remove_search_system(request):
    search_system_data = json.loads(request.body)
    search_system_id = search_system_data.get('search_system_id')
    if search_system_id:
        search_system = SearchSystem.objects.get(id=search_system_id)
        search_system.delete()
        return JsonResponse({'status': 200})

    return JsonResponse({})


def search_project_in_user_projects(request):
    project_name = request.GET.get('project_name')
    user_projects = Project.objects.filter(author=request.user)
    project_links = {}

    if project_name:
        user_projects = user_projects.filter(name__istartswith=project_name)

    for project in user_projects:
        project_links[project.name] = project.get_absolute_url()

    return JsonResponse(project_links, safe=False)


def start_checking_keyword_groups(request):
    current_user = request.user
    current_project_slug = request.GET.get('project_slug')
    check_period = request.GET.get('check_period')
    keyword_group_id = request.GET.get('keyword_group')
    search_system_id = request.GET.get('search_system')
    verification_status = request.GET.get('running_check_status')
    keyword_groups_repository = KeywordGroupsRepository()
    keyword_groups = keyword_groups_repository.get_keyword_groups(current_user=current_user,
                                                                  project_slug=current_project_slug,
                                                                  running_check_status=verification_status)
    position_checker_service = SiteRankingService()
    position_checker_service.check_site_ranking_for_keywords(keyword_groups)

    if all([check_period, keyword_group_id, search_system_id, current_project_slug]):
        period_dates = get_start_and_end_date(check_period)
        rankings_repository = SiteRankingsRepository()
        fields = ('position', 'keywords', 'url_site', 'date_of_check', 'search_system',
                  'keyword_group', 'keyword_groups', 'position_difference')
        converted_site_ratings = rankings_repository.get_site_ratings(period_dates=period_dates,
                                                                      keyword_group_id=keyword_group_id,
                                                                      search_system_id=search_system_id,
                                                                      project_slug=current_project_slug,
                                                                      fields=fields)
        return JsonResponse(converted_site_ratings, safe=False)

    return JsonResponse({'status': 200})


class AddProjectView(LoginRequiredMixin, CreateView):
    template_name = 'project/create.html'
    form_class = ProjectAddingForm

    def form_valid(self, form):
        project = form.save(commit=False)
        project.author = self.request.user
        project.save()
        return HttpResponseRedirect(reverse_lazy('project:add_keywords', kwargs={'slug': project.slug}))


class BasicCreateKeywordsView(CreateView):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        slug = self.kwargs['slug']
        keyword_groups = KeywordGroups.objects.prefetch_related('keywords_set').filter(project__slug=slug)
        context['keyword_groups'] = keyword_groups
        return context

    def get_initial(self):
        return {'slug': self.kwargs['slug']}

    def post(self, request, *args, **kwargs):
        form_data = json.loads(request.body)
        keyword_group_id = form_data.get('keyword_groups')
        keywords = form_data.get('keywords').split('\r\n')
        slug = self.kwargs['slug']
        if keywords and keyword_group_id:
            for keyword in keywords:
                keyword = keyword.strip()
                if keyword:
                    Keywords.objects.create(name=keyword, keyword_groups_id=keyword_group_id)
            # Как лучше здесь оптимизировать работу Django ORM ? Есть вариант с prefetch_related и двумя циклами...
            keyword_groups = KeywordGroups.objects.prefetch_related('keywords_set').filter(project__slug=slug)
            converted_keyword_groups = [keyword_group.as_json('id', 'name', 'keywords') for keyword_group in
                                        keyword_groups]

            return JsonResponse(converted_keyword_groups, safe=False)
        # Какой статус лучше возвращать ??
        return JsonResponse({})


class BasicCreateSearchSystemsView(CreateView):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        slug = self.kwargs['slug']
        context['search_systems'] = SearchSystem.objects.filter(project__slug=slug)
        return context

    def post(self, request, *args, **kwargs):
        try:
            slug = self.kwargs['slug']
            current_project = Project.objects.prefetch_related('keywordgroups_set').get(slug=slug)
            current_project_id = current_project.id
            keyword_groups = current_project.keywordgroups_set.all()
            search_engine = SearchEnginePydantic.parse_raw(request.body)
            search_system_service = SearchSystemService()
            search_system_service.add_search_system(search_engine, current_project_id, keyword_groups)
            search_systems_of_project = SearchSystem.objects.filter(project__slug=slug)
            converted_search_systems = [search_system.as_json('id', 'name', 'city') for search_system in
                                        search_systems_of_project]
            return JsonResponse(converted_search_systems, safe=False)
            # Разобраться c валидацией ??
        except ValidationError as e:
            return JsonResponse(e.json(), safe=False)


class DetailProjectView(DetailView):
    model = Project
    template_name = 'project/detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        slug = self.kwargs['slug']
        period_dates = get_start_and_end_date()
        search_systems = SearchSystem.objects.filter(project__slug=slug)
        latest_search_system = search_systems.first()
        keyword_groups = KeywordGroups.objects.filter(search_systems=latest_search_system)
        first_keyword_group = keyword_groups.first()
        fields = ('position', 'keywords', 'url_site', 'created', 'search_system', 'position_difference')
        rankings_repository = SiteRankingsRepository()
        converted_site_ratings = rankings_repository.get_site_ratings(period_dates=period_dates,
                                                                      keyword_group_id=first_keyword_group,
                                                                      search_system_id=latest_search_system,
                                                                      project_slug=slug,
                                                                      fields=fields)
        context['search_systems'] = search_systems
        context['keyword_groups'] = keyword_groups
        context['site_ratings'] = converted_site_ratings

        return context


class KeywordEditingView(LoginRequiredMixin, BasicCreateKeywordsView):
    template_name = 'keywords/edit.html'
    form_class = KeywordsAddingForm


class KeywordsAddingView(LoginRequiredMixin, BasicCreateKeywordsView):
    template_name = 'keywords/create.html'
    form_class = KeywordsAddingForm


class ProjectEditView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Project
    template_name = 'project/edit.html'
    form_class = ProjectEditingForm
    success_message = 'Общие настройки проекта изменены'

    def get_success_url(self):
        return reverse_lazy('project:project_details', kwargs={'slug': self.kwargs['slug']})


class SearchSystemsAddingView(LoginRequiredMixin, BasicCreateSearchSystemsView):
    template_name = 'search_system/create.html'
    form_class = SearchSystemsAddingForm


class SearchSystemsEditingView(LoginRequiredMixin, BasicCreateSearchSystemsView):
    template_name = 'search_system/edit.html'
    form_class = SearchSystemsAddingForm
