from django import forms

from project.models import (
    Keywords,
    KeywordGroups,
    Project,
    SearchSystem
)


class FormAddingKeywordGroups(forms.ModelForm):
    name = forms.CharField(
        label='Название группы',
        max_length=255
    )

    class Meta:
        model = KeywordGroups
        fields = ('name',)


class KeywordsAddingForm(forms.ModelForm):
    name = forms.Textarea()

    class Meta:
        model = Keywords
        fields = ('name', 'keyword_groups')

    def __init__(self, *args, **kwargs):
        super(KeywordsAddingForm, self).__init__(*args, **kwargs)
        initial = kwargs.get('initial')
        self.fields['keyword_groups'].queryset = KeywordGroups.objects.filter(project__slug=initial['slug'])


class ProjectAddingForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'domain', 'limit_position', 'start_check')


class ProjectEditingForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'domain', 'limit_position', 'start_check')


class SearchSystemsAddingForm(forms.ModelForm):
    class Meta:
        model = SearchSystem
        fields = ('name', 'country', 'city', 'language')
