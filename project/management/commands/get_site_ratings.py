from django.core.management.base import BaseCommand
from project.repositories import KeywordGroupsRepository
from project.services import SiteRankingService


class Command(BaseCommand):
    help = 'Выводит рейтинги сайта'

    def handle(self, *args, **options):
        verification_status = 'automatic_check'
        repository = KeywordGroupsRepository()
        project_keyword_groups = repository.get_keyword_groups(running_check_status=verification_status)
        service = SiteRankingService()
        service.check_site_ranking_for_keywords(keyword_groups=project_keyword_groups,
                                                verification_status=verification_status)
