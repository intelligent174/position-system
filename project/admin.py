from django.contrib import admin

from project.models import (
    Keywords,
    KeywordGroups,
    Project,
    RankingsSite,
    SearchSystem,
)


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'domain', 'limit_position', 'start_check', 'created', 'updated', 'date_of_check',
        'date_of_next_check', 'author', 'slug'
    )
    list_display_links = ('name', 'domain')
    list_filter = ('created', 'limit_position', 'start_check')
    search_fields = ('name', 'domain')
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = Project


@admin.register(Keywords)
class KeywordsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'keyword_groups')
    list_display_links = ('id', 'name',)
    list_filter = ('name', 'keyword_groups')
    search_fields = ('name',)
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = Keywords


@admin.register(KeywordGroups)
class KeywordGroupsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'project', 'keyword_group_search_system')
    list_display_links = ('name',)
    list_filter = ('name', 'project')
    search_fields = ('name',)
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = KeywordGroups


@admin.register(SearchSystem)
class SearchSystemAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'country', 'city', 'geo_id_city', 'language', 'project')
    list_display_links = ('name',)
    list_filter = ('name', 'project')
    search_fields = ('name',)
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = SearchSystem


@admin.register(RankingsSite)
class RankingsSiteAdmin(admin.ModelAdmin):
    list_display = ('id', 'keywords', 'keyword_groups', 'position', 'position_difference', 'url_site', 'updated', 'created', 'search_system')
    list_display_links = ('keywords',)
    list_filter = ('keywords', 'url_site')
    search_fields = ('keywords', 'keyword_groups')
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = RankingsSite
