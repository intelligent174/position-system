from dateutil.relativedelta import relativedelta
from django.db import models
from django.urls import reverse
from django.utils.dateformat import DateFormat
from django.utils.text import slugify
from pydantic import BaseModel, validator, HttpUrl
from urllib.parse import urlparse

from account.models import CustomUser


def get_slug(url: str):
    parsed_url = urlparse(url)
    modified_domain = parsed_url.netloc.replace('.', '_')
    return slugify(modified_domain)


class Keywords(models.Model):
    name = models.TextField('Перечень слов', max_length=10000)
    keyword_groups = models.ForeignKey(
        'KeywordGroups',
        verbose_name='Группа ключевых слов',
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ('id',)
        verbose_name = 'Слово'
        verbose_name_plural = 'Слова'

    def __str__(self):
        return self.name

    def as_json(self, *args):
        return {field_name: getattr(self, field_name) for field_name in args}


class KeywordGroups(models.Model):
    name = models.CharField('Название группы', max_length=255)

    project = models.ForeignKey(
        'Project',
        verbose_name='Проект',
        on_delete=models.CASCADE
    )

    search_systems = models.ManyToManyField(
        'SearchSystem',
        blank=True
    )

    class Meta:
        ordering = ('id',)
        verbose_name = 'Группа ключевых слов'
        verbose_name_plural = 'Группы ключевых слов'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('project:keywords_group_detail', kwargs={'pk': self.id})

    def as_json(self, *args):
        keyword_groups_data = {}

        for field_name in args:
            if field_name == 'keywords':
                keywords_set = self.keywords_set.all()
                if keywords_set:
                    list_keywords = [keyword.as_json('id', 'name') for keyword in keywords_set]
                    keyword_groups_data['keywords'] = list_keywords
            else:
                keyword_groups_data[field_name] = getattr(self, field_name)

        return keyword_groups_data

    def keyword_group_search_system(self):
        return [search_system for search_system in self.search_systems.all()]

    keyword_group_search_system.short_description = 'Поисковые системы группы ключевых слов'


class PositionRequestPydantic(BaseModel):
    project_id: int
    domain: HttpUrl
    search_system_id: int
    keyword_group_id: int
    region_id: int
    query_keywords: str
    position: int = None
    url_site: HttpUrl = None


class Project(models.Model):
    LIMIT_HUNDRED = 100
    LIMIT_TWO_HUNDRED = 200

    LIMIT_CHOICES = (
        (LIMIT_HUNDRED, '100'),
        (LIMIT_TWO_HUNDRED, '200'),
    )

    DATE_CHOICES = (
        ('daily', 'Ежедневно'),
        ('every_three_days', 'Раз в 3 дня'),
        ('weekly', 'Раз в неделю'),
        ('monthly', 'Раз в месяц'),
        ('on_request', 'По запросу'),
    )

    name = models.CharField('Название проекта', max_length=255)
    slug = models.SlugField('Символьный код проекта', max_length=50, unique=True, db_index=True)
    domain = models.URLField(
        'Адрес сайта',
        max_length=50,
        unique=True,
        error_messages={
            'unique': 'Проект с таким адресом уже существует'
        }
    )

    limit_position = models.IntegerField(
        'Глубина сбора позиций',
        choices=LIMIT_CHOICES,
        default=LIMIT_HUNDRED,
        help_text='Сервис будет искать сайт включительно до 100-й или 200-й позиции'
    )

    start_check = models.CharField(
        'Периодичность проверок',
        max_length=30,
        choices=DATE_CHOICES,
        default='weekly',
    )

    created = models.DateTimeField('Дата добавления', auto_now_add=True)

    updated = models.DateTimeField('Дата изменения', auto_now=True)

    date_of_check = models.DateTimeField('Дата проверки', blank=True, null=True)

    date_of_next_check = models.DateTimeField('Дата следующей проверки', blank=True, null=True)

    author = models.ForeignKey(
        CustomUser,
        verbose_name='Автор проекта',
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # if not self.id:
        self.slug = get_slug(self.domain)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('project:project_details', kwargs={'slug': self.slug})

    def get_next_check_date(self, date_of_check):
        if self.start_check == 'daily':
            return date_of_check + relativedelta(days=1)

        elif self.start_check == 'every_three_days':
            return date_of_check + relativedelta(days=3)

        elif self.start_check == 'weekly':
            return date_of_check + relativedelta(weeks=1)

        elif self.start_check == 'monthly':
            return date_of_check + relativedelta(months=1)


class RankingsSite(models.Model):
    position = models.PositiveSmallIntegerField('Позиция сайта', )
    position_difference = models.SmallIntegerField('Разница позиций', null=True)
    project = models.ForeignKey(
        Project,
        verbose_name='Проект',
        on_delete=models.CASCADE
    )
    url_site = models.URLField('Адрес сайта', max_length=255)

    keywords = models.CharField('Ключевые слова', max_length=150)

    keyword_groups = models.ForeignKey(
        KeywordGroups,
        verbose_name='Группы ключевых слов',
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField('Дата добавления', auto_now_add=True)
    updated = models.DateField('Дата изменения', auto_now=True)
    search_system = models.ForeignKey(
        'SearchSystem',
        verbose_name='Поисковая система',
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Позиция сайта'
        verbose_name_plural = 'Позиции сайта'

    def as_json(self, *args):
        site_ranking = {}

        for field_name in args:
            if field_name == 'date_of_check':
                date_format = DateFormat(self.created)
                site_ranking['date_of_check'] = date_format.format('d.m.y')
            elif field_name == 'url_site':
                url_site = getattr(self, field_name)
                url_parsed = urlparse(url_site)
                site_ranking['url_site'] = url_parsed.path
            elif field_name == 'search_system':
                search_system = self.search_system.as_json('name', 'city')
                site_ranking['search_system'] = f'{search_system["name"]}, {search_system["city"]}'
            elif field_name == 'keyword_group':
                site_ranking['keyword_group'] = self.keyword_groups.as_json('name')
            elif field_name == 'keyword_groups':
                search_system_keyword_groups = self.search_system.keywordgroups_set.all()
                site_ranking['keyword_groups'] = [keyword_group.as_json('id', 'name') for keyword_group in
                                                  search_system_keyword_groups]
            else:
                site_ranking[field_name] = getattr(self, field_name)

        return site_ranking


class SearchEnginePydantic(BaseModel):
    name: str
    country: str
    city: str
    city_id: int
    language: str

    @validator('country', 'city')
    def field_value_is_a_string(cls, name):
        if name.isdigit():
            raise ValueError('Значение не должно быть только числом.')
        return name


class SearchSystem(models.Model):
    YANDEX = 'yandex'
    GOOGLE = 'google'
    ENGLISH = 'en'
    RUSSIAN = 'ru'

    LANGUAGE_CHOICES = (
        (ENGLISH, 'english'),
        (RUSSIAN, 'русский'),
    )

    SEARCH_SYSTEM_CHOICES = (
        (YANDEX, 'яндекс'),
        (GOOGLE, 'google'),
    )

    name = models.CharField(
        'Поисковая система',
        max_length=25,
        choices=SEARCH_SYSTEM_CHOICES,
        default=YANDEX
    )

    country = models.CharField('Страна', max_length=50)

    city = models.CharField('Регион', max_length=100)

    geo_id_city = models.IntegerField('Geo id города', )

    language = models.CharField(
        'Язык',
        max_length=25,
        choices=LANGUAGE_CHOICES,
        default=RUSSIAN
    )

    project = models.ForeignKey(
        Project,
        verbose_name='Проект',
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ('id',)
        verbose_name = 'Поисковая система'
        verbose_name_plural = 'Поисковые системы'

    def __str__(self):
        return f'{self.name}, {self.city}'

    def get_absolute_url(self):
        return reverse('project:search_system_detail', kwargs={'pk': self.pk})

    def as_json(self, *args):
        return {field_name: getattr(self, field_name) for field_name in args}
