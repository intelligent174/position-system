from dateutil.relativedelta import relativedelta
from django.db.models import QuerySet
from django.utils import timezone
from json.decoder import JSONDecodeError
from requests import RequestException
from typing import List, Union

from project.models import (
    KeywordGroups,
    PositionRequestPydantic,
    Project,
    RankingsSite,
    SearchEnginePydantic,
    SearchSystem,
)
from utils.yandex_location import YandexLocationApi
from utils.yandex_position import YandexPositionApi


def get_start_and_end_date(check_period: str = '3') -> dict:
    today = timezone.now()
    start_date = None

    if check_period == '3':
        start_date = today - relativedelta(months=3)

    elif check_period == '6':
        start_date = today - relativedelta(months=6)

    elif check_period == '12':
        start_date = today - relativedelta(years=1)

    return {'start_date': start_date, 'end_date': today}


class SearchSystemService:
    @staticmethod
    def add_search_system(search_engine: SearchEnginePydantic, project_id: int,
                          keyword_groups: Union[QuerySet, List[KeywordGroups]]) -> None:
        search_system = SearchSystem.objects.create(
            name=search_engine.name,
            country=search_engine.country,
            city=search_engine.city,
            geo_id_city=search_engine.city_id,
            language=search_engine.language,
            project_id=project_id
        )

        for keyword_group in keyword_groups:
            keyword_group.search_systems.add(search_system)


class SiteRankingService:
    def __init__(self):
        self.position_service = YandexPositionsService()

    def check_site_ranking_for_keywords(self, keyword_groups: List[KeywordGroups],
                                        verification_status: str = None) -> None:
        today = timezone.now()

        for keyword_group in keyword_groups:
            keyword_positions = self.position_service.get_positions_in_yandex(keyword_group)
            self.update_or_create_keyword_rankings(keyword_positions)
            # Сейчас у проекта дата следующей проверки обновляется столько раз сколько у него групп ключевых слов.
            project = keyword_group.project
            project.date_of_check = today
            date_of_next_check = project.date_of_next_check

            if not date_of_next_check:
                self.update_check_date(project, today)

            if verification_status == 'automatic_check':

                if date_of_next_check <= today:
                    self.update_check_date(project, date_of_next_check)

            if verification_status is None:
                self.update_check_date(project, today)

    @staticmethod
    def update_check_date(project: Project, date_of_check) -> None:
        project.date_of_next_check = project.get_next_check_date(date_of_check)
        project.save()

    @staticmethod
    def update_or_create_keyword_rankings(positions_requests: List[PositionRequestPydantic]) -> None:
        today_date = timezone.localdate()

        for position_request in positions_requests:
            data_tuple = RankingsSite.objects.update_or_create(keywords=position_request.query_keywords,
                                                               keyword_groups_id=position_request.keyword_group_id,
                                                               search_system_id=position_request.search_system_id,
                                                               project_id=position_request.project_id,
                                                               updated=today_date,
                                                               defaults={
                                                                   'position': position_request.position,
                                                                   'url_site': position_request.url_site
                                                               })

            rankings_site = data_tuple[0]
            prev_rankings_site = RankingsSite.objects.filter(id__lt=rankings_site.id,
                                                             project_id=rankings_site.project_id,
                                                             keywords=rankings_site.keywords,
                                                             keyword_groups=rankings_site.keyword_groups,
                                                             search_system=rankings_site.search_system
                                                             ).first()

            if prev_rankings_site:
                rankings_site.position_difference = prev_rankings_site.position - rankings_site.position
                rankings_site.save()


class YandexLocationService:

    @staticmethod
    def get_regions(city: str, country: str = '') -> List[dict]:
        api = YandexLocationApi()
        try:
            return api.get_regions(city, country)
        except (JSONDecodeError, RequestException) as e:
            raise YandexLocationServiceError from e


class YandexLocationServiceError(Exception):
    pass


class YandexPositionsService:
    def __init__(self):
        self.api_position = YandexPositionApi()

    def get_positions_in_yandex(self, keyword_group: KeywordGroups) -> List[PositionRequestPydantic]:
        positions_requests = self.get_positions_requests(keyword_group)

        for position_request in positions_requests:
            position, url_site = self.api_position.get_position(position_request)
            position_request.position = position
            position_request.url_site = url_site

        return positions_requests

    @staticmethod
    def get_positions_requests(keyword_group: KeywordGroups) -> List[PositionRequestPydantic]:
        positions_requests = []
        project_id = keyword_group.project_id
        domain = keyword_group.project.domain
        keyword_group_id = keyword_group.id
        keywords_set = list(keyword_group.keywords_set.values_list('name', flat=True))
        search_systems_data = keyword_group.search_systems.values('id', 'geo_id_city')

        for keyword in keywords_set:
            for search_system_data in search_systems_data:
                position_request = PositionRequestPydantic(
                    project_id=project_id,
                    domain=domain,
                    search_system_id=search_system_data["id"],
                    keyword_group_id=keyword_group_id,
                    region_id=search_system_data["geo_id_city"],
                    query_keywords=keyword
                )
                positions_requests.append(position_request)

        return positions_requests
