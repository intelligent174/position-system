from django.urls import path

from project.views import (
    add_keyword_group,
    get_keywords_and_keyword_groups_of_project,
    get_filtered_project_data,
    get_project_search_engines,
    get_regions,
    remove_keyword_or_group_of_keywords,
    remove_search_system,
    search_project_in_user_projects,
    start_checking_keyword_groups,
    KeywordsAddingView,
    AddProjectView,
    KeywordEditingView,
    ProjectEditView,
    DetailProjectView,
    SearchSystemsAddingView,
    SearchSystemsEditingView,
)

app_name = 'project'
urlpatterns = [
    path('ajax/get_keywords_and_keyword_groups_of_project/', get_keywords_and_keyword_groups_of_project,
         name='get_keywords_and_keyword_groups_of_project'),
    path('ajax/get_project_search_engines/', get_project_search_engines,
         name='get_project_search_engines'),
    path('ajax/<slug:slug>/add_keywords_group/', add_keyword_group, name='add_group_of_keywords'),
    path('ajax/filter_project_data/', get_filtered_project_data, name='filter_project_data'),
    path('ajax/regions/', get_regions, name='get_regions'),
    path('ajax/remove_keyword_or_group_of_keywords/', remove_keyword_or_group_of_keywords,
         name='remove_keyword_or_group_of_keywords'),
    path('ajax/remove_search_system/', remove_search_system, name='remove_search_system'),
    path('ajax/start_checking_keyword_groups/', start_checking_keyword_groups,
         name='start_checking_keyword_groups'),
    path('ajax/search_project_in_user_projects/', search_project_in_user_projects,
         name='search_project_in_user_projects'),
    path('<slug:slug>/keywords/add/', KeywordsAddingView.as_view(), name='add_keywords'),
    path('<slug:slug>/search_system/add/', SearchSystemsAddingView.as_view(), name='add_search_system'),
    path('add/', AddProjectView.as_view(), name='project_create'),
    path('<slug:slug>/settings/keywords/edit/', KeywordEditingView.as_view(), name='edit_keywords'),
    path('<slug:slug>/settings/project/edit/', ProjectEditView.as_view(), name='edit_project'),
    path('<slug:slug>/settings/search_systems/edit/', SearchSystemsEditingView.as_view(), name='edit_search_systems'),
    path('<slug:slug>/', DetailProjectView.as_view(), name='project_details'),

]
