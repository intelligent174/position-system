from django.db.models import Q, QuerySet
from django.utils import timezone
from typing import List, Union

from project.models import (
    KeywordGroups,
    RankingsSite,
)


class KeywordGroupsRepository:

    def get_relevant_keyword_groups(self, date=None, current_user: str = None,
                                    project_slug: str = None) -> Union[QuerySet, List[KeywordGroups]]:

        ready_keyword_groups = self.get_keyword_groups_ready_for_check().distinct()

        if current_user:
            ready_keyword_groups = ready_keyword_groups.filter(project__author=current_user)

        if project_slug:
            ready_keyword_groups = ready_keyword_groups.filter(project__slug=project_slug)

        elif date:
            ready_keyword_groups = (
                ready_keyword_groups.filter(Q(project__date_of_check__lte=date) |
                                            Q(project__date_of_check__isnull=True))
                                    .exclude(Q(project__start_check='on_request'))
            )

        return ready_keyword_groups

    def get_keyword_groups(self, current_user: str = None, project_slug: str = None,
                           running_check_status: str = None) -> Union[QuerySet, List[KeywordGroups]]:

        if running_check_status:
            if running_check_status == 'checking_all_user_projects':
                return self.get_relevant_keyword_groups(current_user=current_user)
            today = timezone.now()
            return self.get_relevant_keyword_groups(date=today)

        return self.get_relevant_keyword_groups(current_user=current_user, project_slug=project_slug)

    @staticmethod
    def get_keyword_groups_ready_for_check() -> Union[QuerySet, List[KeywordGroups]]:
        return (
            KeywordGroups.objects.select_related('project').prefetch_related('keywords_set', 'search_systems', )
                                 .filter(search_systems__isnull=False, keywords__isnull=False)
                                 .exclude(keywords__name__exact='')
        )


class SiteRankingsRepository:

    @staticmethod
    def get_site_ratings(period_dates, keyword_group_id: int, search_system_id: int,
                         project_slug: str, fields: tuple) -> List[dict]:
        site_ratings = (
            RankingsSite.objects.select_related('keyword_groups', 'search_system')
                                .prefetch_related('search_system__keywordgroups_set')
                                .filter(project__slug=project_slug,
                                        search_system_id=search_system_id,
                                        keyword_groups=keyword_group_id,
                                        created__lte=period_dates['end_date'],
                                        created__gte=period_dates['start_date'])
        )

        return [site_rating.as_json(*fields) for site_rating in site_ratings]
