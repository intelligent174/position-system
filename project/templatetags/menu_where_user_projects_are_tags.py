from django import template

from project.models import Project

register = template.Library()


@register.inclusion_tag('partials/sidebar.html', takes_context=True)
def get_user_projects(context):
    context['all_user_projects'] = Project.objects.filter(author=context['user'])
    return context


@register.inclusion_tag('partials/sidebar_with_settings.html', takes_context=True)
def get_project(context, slug):
    context['project'] = Project.objects.get(slug=slug)
    return context
